from .models import Competition,Participant
from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField,ReadOnlyField


class CompetitionSerializer(ModelSerializer):
    participant = PrimaryKeyRelatedField(many=True, read_only=True)
    creator = ReadOnlyField(source='creator.username')

    class Meta:
        model = Competition
        fields = '__all__'


class ParticipantSerializer(ModelSerializer):

    class Meta:
        model = Participant
        fields = '__all__'



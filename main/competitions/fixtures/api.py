from django.shortcuts import get_object_or_404
from .models import Fixture, Prediction, Team
from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField,StringRelatedField


class FixtureSerializer(ModelSerializer):
    predictions = PrimaryKeyRelatedField(many=True,read_only=True)
    team_a = StringRelatedField(many=False,read_only=True)
    team_h = StringRelatedField(many=False,read_only=True)

    class Meta:
        model = Fixture
        fields = '__all__'


class PredictionSerializer(ModelSerializer):
    class Meta:
        model = Prediction
        fields = '__all__'


class TeamSerializer(ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'


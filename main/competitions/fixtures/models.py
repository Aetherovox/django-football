from django.db import models
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model as user_model
User = user_model()
# Create your models here.


class Team(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.IntegerField()
    name = models.CharField(max_length = 20)
    short_name = models.CharField(max_length=3)
    strength = models.IntegerField()
    strength_overall_home = models.IntegerField()
    strength_overall_away = models.IntegerField()
    strength_attack_home = models.IntegerField()
    strength_attack_away = models.IntegerField()
    strength_defence_home = models.IntegerField()
    strength_defence_away = models.IntegerField()

    def __str__(self):
        return f'{self.short_name}'


class Fixture(models.Model):
    code = models.PositiveBigIntegerField(primary_key=False)
    event = models.IntegerField()
    finished = models.BooleanField()
    finished_provisional = models.BooleanField()
    kickoff_time = models.CharField(max_length=20)
    minutes = models.PositiveIntegerField()
    provisional_start_time = models.BooleanField()
    started = models.BooleanField()
    team_a = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="team_away")
    team_h = models.ForeignKey(Team, on_delete=models.CASCADE, related_name="team_home")
    team_a_score = models.IntegerField()
    team_h_score = models.IntegerField()
    team_h_difficulty = models.IntegerField()
    team_a_difficulty = models.IntegerField()
    pulse_id = models.PositiveBigIntegerField()


class Prediction(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    fixture = models.ForeignKey(Fixture, on_delete=models.PROTECT,related_name='predictions')
    score_a = models.PositiveSmallIntegerField()
    score_h = models.PositiveSmallIntegerField()
    teamwin = models.IntegerField(choices =(('DRAW',0),('HOME',1),('AWAY',2)))



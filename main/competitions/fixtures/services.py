import requests
import pandas as pd
import math
import numpy as np
from requests_html import AsyncHTMLSession
from collections import defaultdict


def remove_keys(d, keys):
    r = dict(d)
    for key in keys:
        del r[key]
    return r

def pop_keys(d,keys):
    r = dict(d)
    for key in keys:
        r[f'{key}_id'] = r.pop(key)
    return r


def get_fixtures():
    response = requests.get('https://fantasy.premierleague.com/api/fixtures/').json()
    all_fixtures = map(lambda x: remove_keys(x,('stats','id')), response)
    popped = list(map(lambda x: pop_keys(x, ('team_a','team_h')), all_fixtures))
    return popped


def get_teams():
    response = requests.get('https://fantasy.premierleague.com/api/bootstrap-static/').json()["teams"]
    unwanted_keys = ('played','draw','win','loss','points','position','team_division','unavailable','form','pulse_id')
    all_teams = list(map(lambda x: remove_keys(x,unwanted_keys),response))
    return all_teams


def get_results(url):
    asession = AsyncHTMLSession()

    async def get_scores():
        #Get request to pull in url
        r = await asession.get(url)
        await r.html.arender()
        return r

    results = asession.run(get_scores)
    results = results[0]

    #Match Date
    times = results.html.find("div.event__time")
    #Home Team
    home_teams = results.html.find("div.event__participant.event__participant--home")
    #Full Time Score
    Home_Score = results.html.find("div.event__score.event__score--home")
    Away_Score = results.html.find("div.event__score.event__score--away")
    #Away Team
    away_teams = results.html.find("div.event__participant.event__participant--away")
    #Half Time Score - Kept if wanted 
    event_part = results.html.find("div.event__part")

    #Create a dictionary for match results
    dict_res = defaultdict(list)
    
    for ind in range(len(times)):
    
        dict_res['Match_date'].append(times[ind].text)
        dict_res['Home_Team'].append(home_teams[ind].text)
        dict_res['Home_Score'].append(Home_Score[ind].text)
        dict_res['Away_Score'].append(Away_Score[ind].text)
        dict_res['Away_Team'].append(away_teams[ind].text)

    #Return Dictionary
    return dict_res

from django.shortcuts import reverse
from django.views.generic import ListView,DetailView
from .models import Fixture, Team,Prediction
from .model_defaults import fixture_defaults
from rest_framework.viewsets import ModelViewSet
from .api import FixtureSerializer, PredictionSerializer, TeamSerializer
from .services import get_fixtures, get_teams

# Create your views here.

class TeamAPIView(ModelViewSet):
    serializer_class = TeamSerializer

    def get_queryset(self):
        Team.objects.all().delete()
        all_teams = get_teams()
        objects = [Team.objects.create(**team) for team in all_teams]
        return objects


class FixtureAPIView(ModelViewSet):
    serializer_class = FixtureSerializer

    # need to give empty json data default values
    def get_queryset(self):
        # don't want to wipe fixtures, just want to refresh them
        Fixture.objects.all().delete()
        json_data = get_fixtures()
        kwargs = []
        for j in json_data:
            kwargs.append({k: v or fixture_defaults[k] for k,v in j.items()})

        objects = [Fixture.objects.create(**kwarg) for kwarg in kwargs]
        return objects


class PredictionAPIView(ModelViewSet):
    serializer_class = PredictionSerializer

    def get_queryset(self):
        objs = Prediction.objects.all()
        return objs

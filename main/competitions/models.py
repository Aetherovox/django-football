from django.conf.global_settings import AUTH_USER_MODEL
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model as user_model
User = user_model()
from django.shortcuts import reverse

class Participant(models.Model):
    # score in a given competition
    score = models.IntegerField()
    ranking = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    #squad = models.ForeignKey(Squad, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.user.__str__()}'
# Team is something that we create - update , etc.



# Create your models here.
class Competition(models.Model):
    creator = models.ForeignKey(User, on_delete=models.PROTECT)
    title = models.CharField(max_length=100)
    date_started = models.DateTimeField(default=timezone.now)
    participant = models.ForeignKey(Participant,on_delete=models.PROTECT,blank=True,null=True)
    # participant = models.ManyToManyField(User,related_name = 'participant',on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.title}, {self.date_started}'








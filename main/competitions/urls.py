from django.urls import path,include
from .views import Competitions, CompDetailView, UserCompetitions, UserParticipants
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'',Competitions,'competitions')

urlpatterns = [
    path('',include(router.urls))
]

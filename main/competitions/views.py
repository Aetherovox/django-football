from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model as user_model
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView,RetrieveAPIView
from .models import Competition, Participant
from .api import CompetitionSerializer, ParticipantSerializer
User = user_model()
# Create your views here.


class Competitions(ModelViewSet):
    queryset = Competition.objects.all()
    serializer_class = CompetitionSerializer


class CompDetailView(RetrieveAPIView):
    queryset = Competition.objects.all()
    serializer_class = CompetitionSerializer


class UserParticipants(ListAPIView):
    serializer_class = ParticipantSerializer

    def get_queryset(self):
        # restrict this to competitions
        user = get_object_or_404(User,id=self.kwargs.get('user_id'))
        return Participant.objects.filter(user=user)


# TODO: want all competitions for which the user has a participant
class UserCompetitions(ListAPIView):
    serializer_class = CompetitionSerializer

    def get_queryset(self):
        user = self.request.user
        return Competition.objects.filter(participant = user)

    def perform_create(self,serializer):
        serializer.save(owner=self.request.user)

